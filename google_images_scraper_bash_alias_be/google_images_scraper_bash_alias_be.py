# this installs to /usr/bin inside of the directory in which it currently resides
# a copy of the module is also installed in the directory to prevent namespace and versioning issues
import sys
import getopt
from google_images_scraper import scrape_images  # !NOTE! the module is in the directory above here

sys.argv.remove(sys.argv[0])
optlist, args = getopt.getopt(sys.argv, 'hm:d:t:v:')
search_terms = []
verbosity = 0

for option, argument in optlist:
	if option == '-h' or option == '--help':
		print("""
		usage: google_images -m [max_images] -d [directory] *-v [verbosity] -t "[word or phrase]" -t "[more words]"
		
		-h --help:  	this help info
		-m:				the maximum number of images you would like to have scraped and downloaded
		-d:				the directory into which you would a a directory of images to be created
		-t:  			search terms; use this in front of every word or phrase in order to differentiate different searches
						* requires double quotes for searches that have spaces or multiple words
		-v: 			verbosity is optional; default 0
		
		See https://github.com/ABlueTortosie30/google_images_scraper/README.md for more info. 
		""")

	if option == '-m':
		max_images = argument

	if option == '-d':
		dir = argument
		if dir[-1] != '/':
			dir = dir + '/'

	if option == '-v':
		verbosity = int(argument)

	if option == '-t':
		search_terms.append(argument)

for term in search_terms:
	print(max_images, dir, term, verbosity)
	scrape_images(max_images, dir, term, verbosity=verbosity)
