import shutil

alias_gone = False

alias_path = input('\nPlease enter the absolute path to you .bash_aliases file.  ie:  /home/user/.bash_aliases\n')

with open(alias_path, 'r') as f:
	aliases = f.readlines()

for line in aliases:
	if 'alias google_images=' in line:
		aliases.remove(line)
		with open(alias_path, 'w') as f:
			f.writelines(aliases)
		alias_gone = True
		print('\nBash alias removed from .bash_aliases.\n')

if not alias_gone:
	print('\nError:  Failed to remove alias from .bash_aliases.\n')

shutil.rmtree('/usr/bin/google_images_scraper_bash_alias_be')
print('\nBackend script removed\n')
