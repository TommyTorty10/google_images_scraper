import shutil


class AnswerError(Exception):
	"""You entered something unwanted or incorrect"""
	print('\nenter either y or n; case insensitive')
	pass


print('\nPlease answer the following questions correctly to install.')
bash_alias = input('\nDo you want to add the Bash alias to $HOME/.bash_aliases as well? \n'
				   ' *Note, you must have a .bash_aliases file configured with your .bashrc file \n'
				   'y/n \n')
bash_alias = bash_alias.lower()
if bash_alias != 'y':
	if bash_alias != 'n':
		raise AnswerError

if bash_alias == 'y':
	alias_path = input('\nPlease enter the path to you .bash_aliases file.  ie:  /home/user/.bash_aliases\n')

	with open(alias_path, 'a') as f:
		writouptput = f.write("""\nalias google_images="sudo python3 /usr/bin/google_images_scraper_bash_alias_be/google_images_scraper_bash_alias_be.py"\n""")
	print('\nBash alias successfully installed')

	movement = shutil.copytree('google_images_scraper_bash_alias_be', '/usr/bin/google_images_scraper_bash_alias_be')
	shutil.copytree('google_images_scraper', '/usr/bin/google_images_scraper_bash_alias_be/google_images_scraper')
	print('\nPython script for alias successfully installed at:', movement)

color = input('\nWhat is your favorite color?  ')
trek = input('\nKirk or Picard?  ')
if trek == 'Janeway':
	print('Correct!')
pet = input('\nCats or Dogs?  ')
if pet == 'Tortoises':
	print('Correct!')
