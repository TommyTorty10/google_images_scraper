from bs4 import BeautifulSoup as bs
from urllib import request
from re import sub
import os
from imghdr import what


def download_image(search_term, soup, search_dir, max_images=3, verbosity=0):
	if search_dir[-1] != '/':
		search_dir = search_dir+'/'
	search_dir = search_dir+search_term
	print(search_dir)

	images = []
	max_images = int(max_images)

	# find URL's to image files
	for a in soup.find_all('img'):
		line = str(a)
		if 'data-src' in line:
			line = line.split('"')
			for i in line:
				if 'http' in i:
					images.append(i)

	# download the images
	try:
		os.mkdir(search_dir)  # make individual dir for each image
	except Exception as e:
		print('Failed to make directory', search_dir+'.')
		print(e)
	counter = 0
	for image in images:
		if counter == max_images:
			break
		try:
			raw_img = request.urlopen(image).read()
			with open(search_dir+"/"+'image'+str(counter), 'wb') as f:
				f.write(raw_img)

			filetype = what(search_dir+"/"+'image'+str(counter))
			os.rename(search_dir+"/"+'image'+str(counter), search_dir+"/"+'image'+str(counter)+'.'+filetype)

			if verbosity == 3:
				print(image, 'downloaded successfully')
			counter += 1

		except Exception as e:
			if verbosity > 0:
				print(e)

	if verbosity > 1:
		print(counter, 'images downloaded for', search_term)


def scrape_images(max_images, search_dir, *image_searches, verbosity=0):
	# iterate through the image_searches
	for search in image_searches:
		# clean up search
		search = search.strip()
		search = sub(r"\s+", '+', search)

		url = 'https://www.google.com/search?q='\
			  + search + '+shape&safe=off&site=&tbm=isch&source=hp&client=ubuntu'  # search url

		# The default header reveal that python is scraping, so new a header is made to masquerade as a normal user.
		header = {'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) "
								"Chrome/43.0.2357.134 Safari/537.36"}

		# get webpage of images
		ask = request.Request(url, headers=header)
		sauce = request.urlopen(ask).read()
		soup = bs(sauce, 'lxml')

		# parse soup and download images
		download_image(search, soup, search_dir=search_dir, max_images=max_images, verbosity=verbosity)

		if verbosity == 1:
			print(search, 'has been collected')
