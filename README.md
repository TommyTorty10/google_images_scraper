# Python 3 Google Images Scraper (In Development)
## Features
* Takes keyword arguments and downloads *x* images
* Includes Bash script for easy use through terminal

### Table of Contents
1. [Installation](#installation)
2. [Module Usage](#usage)
3. [Bash Alias Usage](#bash-alias)
4. [Uninstallation](#uninstallationremoval)
5. [FAQ and Miscellanious Info](#other-info)

## Installation
#### Requirements
* Python 3.6 or greater
* A Bash terminal

#### Installation Instructions
* open a terminal
* `git clone https://github.com/ABlueTortoise30/google_images_scraper.git`
* navigate into `google_images_scraper/` (the directory of the repo, not the module)
* run `sudo python3 install.py` first, and answer all of it's questions
    * the bash alias can allow you to run a command from a bash terminal to pull images off of Google Images
    * you must have a `.bash_aliases` file correctly configured in your home directory for the alias to be installed and 
    function properly
* `pip3 install .`

## Usage
#### Python Module
Below is an example of how to use the image scraper.  It is included in the repo as `example_usage.py`.  The first 
argument to `scrape_images` is the maximum number of images to download for each search.  The second argument is the 
directory to save everything to.  Absolute paths generally work best.  If you provide a path to a directory that does 
not exist, the module will attempt to create a new directory. The rest of the positional arguments are words 
and phrases to search for.

```python
#!/usr/bin/env python3
from google_images_scraper import scrape_images

replace the example filepath with your own absolute filepath and delete this line
scrape_images(5, '/home/user/my_new_images', 'kittens', 'puppies', 'cute animals', verbosity=3)
```

#### scrape_images()
The `scrape_images()` function will do a Google Image search for every argument given and save the first images to 
a separate folder for each argument.  

Parameters:
* `max_images`:
    * type: int
    * `scrape_images()` will not download more than `max_images` number of images, however it may return less than 
    `max_images` based on the results Google provides.

* `search_dir`:
    * type: string
    * `search_dir` is the path to the directory into which the searches for images are to be saved.  If the directory 
    given does not exits, a new one will be created. Each Google Images search will create a separate subdirectory 
    within `search_dir` and save the images in the new subdirectory.

* `verbosity`:
    * type: int
    * `verbosity = 0` returns no output beyond the images downloaded
    * `verbosity = 1` returns every search completed and possible errors in the download process
    * `verbosity = 2` returns the number of images downloaded in a search
    * `verbosity = 3` returns the number of images downloaded in a search and a message reporting each successful image 
    download

* `*image_searchs`:
    * type: str
    * `*image_searchs` is the collections of arguments that each represent a Google Images search.  Each string provided 
    will be searched for on Google Images.

#### Bash Alias
The bash alias acts like any othe command in the terminal.  You can use `google_images -h` to run the alias and display 
helpful information which is as follows:
```text
		-h --help:      this help info
		-m:             the maximum number of images you would like to have scraped and downloaded
		-d:             the directory into which you would a a directory of images to be created
		-t:             search terms; use this in front of every word or phrase in order to differentiate different searches
		-v:             verbosity is optional; default 0
```

## Uninstallation/Removal
There is an `uninstall.py` script to remove the bash_alias and its associated backend.  The module must be uninstalled 
with pip.

#### Steps to Remove a Complete Installation
* `sudo python3 unintall.py`
* `pip3 uninstall google_images_scraper`

## Other Info
* `scrape_images` may return less than `max_images` based on the results Google provides.  

* Since there are countless names for images, and some strings could potentially be harmful to certain programs, each 
image is renamed sequentially starting at 0 following the format image0.jpeg, image1.jpeg, et cetera.
